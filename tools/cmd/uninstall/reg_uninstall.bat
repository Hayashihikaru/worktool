if "%PROCESSOR_ARCHITECTURE%" == "x86" (
    SET REG_UNINSTALL_KEY=HKLM\SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall
) else (
    SET REG_UNINSTALL_KEY=HKLM\SOFTWARE\Wow6432Node\Microsoft\Windows\CurrentVersion\Uninstall
)
for /f "tokens=1,2*" %%A in ('reg query "%REG_UNINSTALL_KEY%"') do (
    reg query "%%A" /v DisplayName 2>NUL >> uninstall_regedit.txt 
)
