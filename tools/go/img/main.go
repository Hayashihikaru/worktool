package main

import (
	//"bytes"
	"bufio"
	"fmt"
	"image"
	"image/color"
	"image/jpeg"
	"log"
	"os"
	//"reflect"
)

func main() {
	inputpaths := fromFile("inputimages.config")
	outputpaths := fromFile("outputimages.config")
	for i := 0; i < len(inputpaths); i++ {

		//Load JPEG Image File Path
		inputFile, err := os.Open(inputpaths[i])
		if err != nil {
			log.Fatal(err)
		}
		inputImage, _, err := image.Decode(inputFile)
		if nil != err {
			fmt.Println(err)
		}
		defer inputFile.Close()

		// Output Image
		outputFile, err := os.Create(outputpaths[i])
		if nil != err {
			fmt.Println(err)
		}

		//Check  Width Height
		//var img_h, img_w int = checkWidthHeightImage(inputFile)

		outputImage := convertToOutYImage(inputImage)
		option := &jpeg.Options{Quality: 100}
		err = jpeg.Encode(outputFile, outputImage, option) // エンコード

		if nil != err {
			fmt.Println(err)
		}

		defer outputFile.Close()
		fmt.Println("Finish!")
	}
}

//ファイルパスを指定すると一行ずつファイルを読んで”paths”配列に格納する。
func fromFile(filePath string) []string {
	// ファイルを開く
	f, err := os.Open(filePath)
	if err != nil {
		//Error表示
		fmt.Fprintf(os.Stderr, "File %s could not read: %v\n", filePath, err)
		os.Exit(1)
	}

	// 関数return時に閉じる
	defer f.Close()

	// Scannerで読み込む
	// paths := []string{}
	paths := make([]string, 0, 100) // ある程度行数が事前に見積もれるようであれば、makeで初期capacityを指定して予めメモリを確保しておくことが望ましい
	scanner := bufio.NewScanner(f)
	for scanner.Scan() {
		// appendで追加
		paths = append(paths, scanner.Text())
	}
	if serr := scanner.Err(); serr != nil {
		fmt.Fprintf(os.Stderr, "File %s scan error: %v\n", filePath, err)
	}

	return paths
}

func convertToOutYImage(inputImage image.Image) image.Image {
	rect := inputImage.Bounds()
	width := rect.Size().X
	height := rect.Size().Y
	rgba := image.NewRGBA(rect)

	for x := 0; x < width; x++ {
		for y := 0; y < height; y++ {
			var col color.RGBA64 //RGBAじゃダメ
			// 座標(x,y)のR, G, B, α の値を取得
			r, g, b, a := inputImage.At(x, y).RGBA()

			//RGB to BT.709 YCbCr Convert
			// それぞれを重み付けして足し合わせる(NTSC 系加重平均法)
			outR := float32(uint16(r)) * 0.298912
			outG := float32(uint16(g)) * 0.58611
			outB := float32(uint16(b)) * 0.114478
			mono := uint16(outR + outG + outB)
			col.R = mono
			col.G = mono
			col.B = mono
			col.A = uint16(a)
			rgba.Set(x, y, col)
		}
	}
	return rgba.SubImage(rect)
}
